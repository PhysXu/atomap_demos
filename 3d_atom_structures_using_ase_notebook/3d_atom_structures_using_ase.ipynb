{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Atomap tutorial: 3D atom structures using ASE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For more details see the open access article: **Atomap: a new software tool for the automated analysis of atomic resolution images using two-dimensional Gaussian fitting**. https://dx.doi.org/10.1186/s40679-017-0042-5"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial shows how to use Atomap to import and export atomic structures to the Python library [Atomic Simulation Environment (ASE)](https://wiki.fysik.dtu.dk/ase/), which allows for 3D atomic models in ASE to be turned into lattices in Atomap, and lattices in Atomap to be turned into 3D atomic models.\n",
    "\n",
    "For more information about this functionality, see the Atomap webpage's section on [Working with atomic models](https://atomap.org/working_with_atomic_models.html)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This notebook assumes some familiarity with Atomap (and HyperSpy), for a more thorough introduction see the user guide at https://atomap.org/finding_atom_lattices.html or the introductory notebook at https://gitlab.com/atomap/atomap_demos/blob/release/introduction_to_atomap.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Importing the libraries\n",
    "\n",
    "Firstly, we must set the plotting toolkit:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false,
    "tags": [
     "nbval-skip"
    ]
   },
   "outputs": [],
   "source": [
    "%matplotlib notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import atomap.api as am"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3D atoms model to Atom_Lattice"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets first generate a 3D atomic model using the ASE library."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from ase.cluster import Octahedron\n",
    "atoms = Octahedron('Ag', 10, cutoff=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To visualize the 3D model, we use the in-built ASE viewer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.visualize import view"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view(atoms)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To convert it to an `Atom_Lattice` Atomap object, use `ase_to_atom_lattice`. This projects the `atoms` object in the Z-direction. Ergo the Z-direction is the \"electron beam direction\".\n",
    "\n",
    "Note that the atom positions in the `Atom_Lattice` will be shifted in relation to the positions in the `atoms` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "atom_lattice = am.convert_ase.ase_to_atom_lattice(atoms)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "atom_lattice.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To increase the \"size\" of the atomic columns, `gaussian_blur` is used"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "atom_lattice = am.convert_ase.ase_to_atom_lattice(atoms, gaussian_blur=10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "atom_lattice.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2D atomic lattice to 3D ASE model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get a 3D ASE model from an Atom_Lattice object, we first need to tell Atomap where the atoms are located in the Z-direction.\n",
    "\n",
    "We use the `get_perovskite_001_atom_lattice` as an example. Here, the 3D information is deactivated with the `set_element_info=False` parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "atom_lattice = am.dummy_data.get_perovskite_001_atom_lattice(set_element_info=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, the scale must be set for the sublattice, in Ångstrøm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "atom_lattice.set_scale(scale=0.2, units=\"Å\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then the Z-position is set for every atomic column in the sublattice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sublattice_A = atom_lattice.sublattice_list[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The values here must be in Ångstrøm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sublattice_A.set_element_info('Sr', [0., 4., 8., 12.])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If there are different elements in the atom column, each element and its position must be specified:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sublattice_B = atom_lattice.sublattice_list[1]\n",
    "sublattice_B.set_element_info(['O', 'Ti', 'O', 'Ti', 'O', 'Ti', 'O'], [0., 2., 4., 6., 8., 10., 12.])\n",
    "atoms_001 = atom_lattice.convert_to_ase()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To visualize the 3D model. To change the size of the atom radii: `View -> Settings -> Scale atomic radii`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view(atoms_001)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Exporting 3D model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.io import write"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "write(\"perovskite_001.cif\", atoms_001)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.1+"
  },
  "widgets": {
   "state": {},
   "version": "1.1.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
